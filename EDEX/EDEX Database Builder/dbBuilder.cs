﻿using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EDEX_Database_Builder
{
    public partial class dbBuilder : Form
    {

        const string sqlParams = "Data Source=:memory:;Version=3;Journal Mode=Off;Synchronous=Off;Page Size=4096;Cache Size=16777216;";

        SQLiteConnection zapis = new SQLiteConnection("Data Source=galaxy.sqlite;Version=3;Journal Mode=Off;Synchronous=Off;Page Size=4096;Cache Size=16777216;");
        SQLiteConnection m_dbConnection;

        PrivateFontCollection pfc = new PrivateFontCollection();

        long per_s = 0;
        long lp = 0;
        int lastPercent = -1;

        private Timer timer1;
        public void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 1000;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            status.Text = lp.ToString("#,##0") + " total\n" + per_s.ToString("#,##0") + "/s";



            per_s = 0;
        }

        public dbBuilder()
        {
            InitializeComponent();
        }

        private void start_Click(object sender, EventArgs e)
        {
            if (File.Exists(@"bodies.jsonl") && File.Exists(@"systems.csv"))
            {
                try
                {
                    builder.RunWorkerAsync();
                    start.Enabled = false;
                }
                catch { }
            }
            else
            {
                MessageBox.Show("bodies.jsonl or systems.csv not found.\nMake sure you read the instruction prior to generating database.", "Missing files", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            
        }




        public void echo(string tekst)
        {
            console.Invoke((MethodInvoker)delegate
            {
                console.SelectionStart = console.Text.Length;
                console.ScrollToCaret();
                console.SelectedText = tekst + "\n";
                console.SelectionStart = console.Text.Length;
                console.ScrollToCaret();
            });
        }




        private void builder_DoWork(object sender, DoWorkEventArgs e)
        {
            echo("\n[1/3] Creating database");
            SQLiteConnection.CreateFile("galaxy.sqlite");
            echo("Database ready");

            zapis.Open();
            m_dbConnection = new SQLiteConnection(sqlParams);
            m_dbConnection.Open();

            createTablePlanets();
            createTableSystems();

            zapis.Close();
            echo("\nDatabase finished!");
        }



        /****************************************************************************
        *   PLANETY
        *****************************************************************************/
        public void createTablePlanets()
        {
            try
            {
                lp = 0;
                echo("\n[2/4] Preparing bodies table");


                string sql = "CREATE TABLE bodies (system_id INT, name TEXT, type_id INT)";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                Stopwatch sw = Stopwatch.StartNew(); //!! do wyjebki na koniec

                string skleja = "BEGIN TRANSACTION;";

                using (WebClient client = new WebClient())
                using (Stream stream = client.OpenRead(@"bodies.jsonl"))
                using (StreamReader streamReader = new StreamReader(stream))
                using (JsonTextReader reader = new JsonTextReader(streamReader))
                {
                    reader.SupportMultipleContent = true;

                    var serializer = new JsonSerializer();
                    while (reader.Read())
                    {
                        if (reader.TokenType == JsonToken.StartObject)
                        {
                            Body c = serializer.Deserialize<Body>(reader);

                            if (c.type_id == null)
                                c.type_id = "0";

                            //tu pisanie do sql
                            skleja += "insert into bodies (system_id, name, type_id) values (" + c.system_id + ", '" + c.name.Replace('\'', '-') + "', " + c.type_id + "); ";


                            if (lp % 200 == 0) //200
                            {
                                SQLiteCommand cc = new SQLiteCommand(skleja + "", m_dbConnection); //COMMIT;
                                cc.ExecuteNonQuery();
                                skleja = ""; //BEGIN TRANSACTION;
                            }


                            if (lp % 10000 == 0) //200
                            {
                                skleja += "COMMIT;BEGIN TRANSACTION;";
                            }


                            lp++;
                            per_s++;
                        }
                    }




                    //dopisanie tych, które zostały na samym końcu w niedokończonym cyklu
                    try
                    {
                        SQLiteCommand cc = new SQLiteCommand(skleja + "COMMIT;", m_dbConnection);
                        cc.ExecuteNonQuery();
                        skleja = "BEGIN TRANSACTION;";
                    }
                    catch { }

                }

                echo("Bodies table ready");
                //m_dbConnection.Close();

                /*echo("Bodies table ready, writting to HDD");
                // save memory db to file
                m_dbConnection.BackupDatabase(zapis, "main", "main", -1, null, 0);
                
                m_dbConnection.Close();
                echo("Bodies table saved");*/
            }
            catch (Exception ee)
            {
                echo("Exception while building bodies table:\n" + ee);
            }

        }








        /****************************************************************************
        *   SYSTEMY
        *****************************************************************************/
        public void createTableSystems()
        {
            try
            {
                echo("\n[3/4] Preparing systems table");


                /*m_dbConnection = new SQLiteConnection(sqlParams);
                m_dbConnection.Open();*/



                string sql = "CREATE TABLE systems (id INT, name TEXT)";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                lp = 1;


                string skleja = "BEGIN TRANSACTION;";

                TextFieldParser parser = new TextFieldParser(@"systems.csv");
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                parser.ReadFields(); //zrzut nagłówków



                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();

                    if (fields[0] == "")
                        fields[0] = "-1";

                    skleja += "insert into systems(id, name) values(" + fields[0] + ", '" + fields[2].Replace('\'', '-') + "');";


                    if (lp % 200 == 0) //200
                    {
                        SQLiteCommand cc = new SQLiteCommand(skleja + "COMMIT;", m_dbConnection);
                        cc.ExecuteNonQuery();
                        skleja = "BEGIN TRANSACTION;";
                    }

                    lp++;
                    per_s++;

                }


                //dopisanie tych, które zostały na samym końcu w niedokończonym cyklu
                try
                {
                    SQLiteCommand cc = new SQLiteCommand(skleja + "COMMIT;", m_dbConnection);
                    cc.ExecuteNonQuery();
                    skleja = "BEGIN TRANSACTION;";
                }
                catch { }


                parser.Close();



                echo("Systems table ready");

                echo("\n[4/4] Writting database to HDD");
                // save memory db to file
                m_dbConnection.BackupDatabase(zapis, "main", "main", -1, null, 0);

                m_dbConnection.Close();

                echo("Database saved");
            }
            catch (Exception ee)
            {
                echo("Exception while building bodies table:\n" + ee);
            }
        }




        public void loadFont()
        {
            int fontLength = Properties.Resources.Waukegan_LDO.Length;
            byte[] fontdata = Properties.Resources.Waukegan_LDO;
            System.IntPtr data = Marshal.AllocCoTaskMem(fontLength);
            Marshal.Copy(fontdata, 0, data, fontLength);
            pfc.AddMemoryFont(data, fontLength);

            console.Font = new Font(pfc.Families[0], console.Font.Size);
            start.Font = new Font(pfc.Families[0], start.Font.Size);
            status.Font = new Font(pfc.Families[0], status.Font.Size);
            logo.Font = new Font(pfc.Families[0], logo.Font.Size);
            logo2.Font = new Font(pfc.Families[0], logo2.Font.Size);
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            loadFont();

            InitTimer();
            echo("READ THIS BEFORE STARTING\n\nBuilding database might take few hours, depending on your PC performance.\n" +
                "You need to download \"systems.csv\" and \"bodies.jsonl\" from this website: https://eddb.io/api.\n" +
                "and put them in EDEX installation directory. You can delete them once database is generated.\n" +
                "As of August 2017 there are about 8,000,000 bodies in about 12,500,000 systems.\n" +
                "Operation will require about 1.5GB RAM.\n" +
                "EDEX has to be closed during the operation." +
                "If you stop the operation or an error occurs, you won't be able to resume and will have to start over.\n" +
                "\n\nClick \"Build database\" when ready.");
        }

        private void console_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }
    }
}

class Body
{
    public string id { get; set; }
    public string created_at { get; set; }
    public string updated_at { get; set; }
    public string name { get; set; }
    public string system_id { get; set; }
    public string group_id { get; set; }
    public string group_name { get; set; }
    public string type_id { get; set; }
    public string type_name { get; set; }
}

