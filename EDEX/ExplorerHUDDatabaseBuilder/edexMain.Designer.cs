﻿namespace EDEX
{
    partial class edexMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(edexMain));
            this.dbScavenger = new System.ComponentModel.BackgroundWorker();
            this.hidder = new System.Windows.Forms.Timer(this.components);
            this.mainView = new System.Windows.Forms.Panel();
            this.totalv = new System.Windows.Forms.Label();
            this.loading = new System.Windows.Forms.PictureBox();
            this.bodiesContainer = new System.Windows.Forms.Panel();
            this.close = new System.Windows.Forms.Button();
            this.settings = new System.Windows.Forms.Button();
            this.bodiesValue = new System.Windows.Forms.Label();
            this.simrun = new System.Windows.Forms.Button();
            this.simjump = new System.Windows.Forms.Button();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.checkInput = new System.Windows.Forms.Button();
            this.dbgPanel = new System.Windows.Forms.Panel();
            this.dbgMode = new System.Windows.Forms.Label();
            this.console = new System.Windows.Forms.RichTextBox();
            this.fsRefresh = new System.Windows.Forms.Timer(this.components);
            this.mainView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loading)).BeginInit();
            this.dbgPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dbScavenger
            // 
            this.dbScavenger.DoWork += new System.ComponentModel.DoWorkEventHandler(this.dbScavenger_DoWork);
            // 
            // hidder
            // 
            this.hidder.Interval = 15000;
            this.hidder.Tick += new System.EventHandler(this.hidder_Tick);
            // 
            // mainView
            // 
            this.mainView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(11)))), ((int)(((byte)(0)))));
            this.mainView.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mainView.BackgroundImage")));
            this.mainView.Controls.Add(this.totalv);
            this.mainView.Controls.Add(this.loading);
            this.mainView.Controls.Add(this.bodiesContainer);
            this.mainView.Controls.Add(this.close);
            this.mainView.Controls.Add(this.settings);
            this.mainView.Controls.Add(this.bodiesValue);
            this.mainView.Location = new System.Drawing.Point(12, 12);
            this.mainView.Name = "mainView";
            this.mainView.Size = new System.Drawing.Size(1108, 112);
            this.mainView.TabIndex = 6;
            // 
            // totalv
            // 
            this.totalv.BackColor = System.Drawing.Color.Transparent;
            this.totalv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.totalv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.totalv.Location = new System.Drawing.Point(682, 89);
            this.totalv.Name = "totalv";
            this.totalv.Size = new System.Drawing.Size(423, 23);
            this.totalv.TabIndex = 10;
            this.totalv.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // loading
            // 
            this.loading.BackColor = System.Drawing.Color.Transparent;
            this.loading.Image = ((System.Drawing.Image)(resources.GetObject("loading.Image")));
            this.loading.Location = new System.Drawing.Point(535, 30);
            this.loading.Name = "loading";
            this.loading.Size = new System.Drawing.Size(32, 29);
            this.loading.TabIndex = 9;
            this.loading.TabStop = false;
            this.loading.Visible = false;
            // 
            // bodiesContainer
            // 
            this.bodiesContainer.BackColor = System.Drawing.Color.Transparent;
            this.bodiesContainer.Location = new System.Drawing.Point(14, 15);
            this.bodiesContainer.Name = "bodiesContainer";
            this.bodiesContainer.Size = new System.Drawing.Size(1050, 68);
            this.bodiesContainer.TabIndex = 8;
            // 
            // close
            // 
            this.close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.close.BackColor = System.Drawing.Color.Transparent;
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.close.FlatAppearance.BorderSize = 0;
            this.close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.close.Image = ((System.Drawing.Image)(resources.GetObject("close.Image")));
            this.close.Location = new System.Drawing.Point(1080, 3);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(25, 25);
            this.close.TabIndex = 1;
            this.close.UseVisualStyleBackColor = false;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // settings
            // 
            this.settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.settings.BackColor = System.Drawing.Color.Transparent;
            this.settings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.settings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.settings.FlatAppearance.BorderSize = 0;
            this.settings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.settings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settings.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.settings.Image = ((System.Drawing.Image)(resources.GetObject("settings.Image")));
            this.settings.Location = new System.Drawing.Point(1080, 34);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(25, 25);
            this.settings.TabIndex = 0;
            this.settings.UseVisualStyleBackColor = false;
            this.settings.Click += new System.EventHandler(this.settings_Click);
            // 
            // bodiesValue
            // 
            this.bodiesValue.BackColor = System.Drawing.Color.Transparent;
            this.bodiesValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bodiesValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.bodiesValue.Location = new System.Drawing.Point(0, 89);
            this.bodiesValue.Name = "bodiesValue";
            this.bodiesValue.Size = new System.Drawing.Size(1108, 20);
            this.bodiesValue.TabIndex = 5;
            this.bodiesValue.Text = "Ready. Jump to begin.";
            this.bodiesValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // simrun
            // 
            this.simrun.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.simrun.Location = new System.Drawing.Point(4, 90);
            this.simrun.Name = "simrun";
            this.simrun.Size = new System.Drawing.Size(295, 23);
            this.simrun.TabIndex = 11;
            this.simrun.Text = "Simulate game launch";
            this.simrun.UseVisualStyleBackColor = true;
            this.simrun.Click += new System.EventHandler(this.simrun_Click);
            // 
            // simjump
            // 
            this.simjump.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.simjump.Location = new System.Drawing.Point(4, 61);
            this.simjump.Name = "simjump";
            this.simjump.Size = new System.Drawing.Size(295, 23);
            this.simjump.TabIndex = 10;
            this.simjump.Text = "Simulate jump to last system from journal";
            this.simjump.UseVisualStyleBackColor = true;
            this.simjump.Click += new System.EventHandler(this.simjump_Click);
            // 
            // nameInput
            // 
            this.nameInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.nameInput.Location = new System.Drawing.Point(3, 34);
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(119, 20);
            this.nameInput.TabIndex = 2;
            this.nameInput.Text = "Sol";
            // 
            // checkInput
            // 
            this.checkInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.checkInput.Location = new System.Drawing.Point(128, 32);
            this.checkInput.Name = "checkInput";
            this.checkInput.Size = new System.Drawing.Size(171, 23);
            this.checkInput.TabIndex = 3;
            this.checkInput.Text = "Show data on system";
            this.checkInput.UseVisualStyleBackColor = true;
            this.checkInput.Click += new System.EventHandler(this.checkInput_Click);
            // 
            // dbgPanel
            // 
            this.dbgPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.dbgPanel.Controls.Add(this.dbgMode);
            this.dbgPanel.Controls.Add(this.console);
            this.dbgPanel.Controls.Add(this.nameInput);
            this.dbgPanel.Controls.Add(this.simrun);
            this.dbgPanel.Controls.Add(this.checkInput);
            this.dbgPanel.Controls.Add(this.simjump);
            this.dbgPanel.Location = new System.Drawing.Point(12, 124);
            this.dbgPanel.Name = "dbgPanel";
            this.dbgPanel.Size = new System.Drawing.Size(1108, 117);
            this.dbgPanel.TabIndex = 12;
            // 
            // dbgMode
            // 
            this.dbgMode.AutoSize = true;
            this.dbgMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dbgMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.dbgMode.Location = new System.Drawing.Point(3, 6);
            this.dbgMode.Name = "dbgMode";
            this.dbgMode.Size = new System.Drawing.Size(130, 20);
            this.dbgMode.TabIndex = 13;
            this.dbgMode.Text = "DEBUG MODE";
            // 
            // console
            // 
            this.console.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            this.console.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.console.ForeColor = System.Drawing.Color.White;
            this.console.Location = new System.Drawing.Point(305, 3);
            this.console.Name = "console";
            this.console.ReadOnly = true;
            this.console.Size = new System.Drawing.Size(800, 111);
            this.console.TabIndex = 12;
            this.console.Text = "";
            // 
            // fsRefresh
            // 
            this.fsRefresh.Enabled = true;
            this.fsRefresh.Interval = 1000;
            this.fsRefresh.Tick += new System.EventHandler(this.fsRefresh_Tick);
            // 
            // edexMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1132, 243);
            this.Controls.Add(this.dbgPanel);
            this.Controls.Add(this.mainView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "edexMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EDEX";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Black;
            this.Load += new System.EventHandler(this.edexMain_Load);
            this.mainView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loading)).EndInit();
            this.dbgPanel.ResumeLayout(false);
            this.dbgPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button settings;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.Button checkInput;
        private System.ComponentModel.BackgroundWorker dbScavenger;
        private System.Windows.Forms.Label bodiesValue;
        private System.Windows.Forms.Panel mainView;
        private System.Windows.Forms.Panel bodiesContainer;
        private System.Windows.Forms.PictureBox loading;
        private System.Windows.Forms.Timer hidder;
        private System.Windows.Forms.Button simjump;
        private System.Windows.Forms.Button simrun;
        private System.Windows.Forms.Label totalv;
        private System.Windows.Forms.Panel dbgPanel;
        private System.Windows.Forms.RichTextBox console;
        private System.Windows.Forms.Label dbgMode;
        private System.Windows.Forms.Timer fsRefresh;
    }
}