﻿namespace EDEX
{
    partial class settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(settings));
            this.sound = new System.Windows.Forms.CheckBox();
            this.hide = new System.Windows.Forms.CheckBox();
            this.logo = new System.Windows.Forms.Label();
            this.dbb = new System.Windows.Forms.Button();
            this.logo2 = new System.Windows.Forms.Label();
            this.about = new System.Windows.Forms.Button();
            this.donate = new System.Windows.Forms.Button();
            this.ver = new System.Windows.Forms.Label();
            this.onlydetect = new System.Windows.Forms.CheckBox();
            this.transp = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // sound
            // 
            this.sound.AutoSize = true;
            this.sound.BackColor = System.Drawing.Color.Transparent;
            this.sound.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sound.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.sound.Location = new System.Drawing.Point(12, 198);
            this.sound.Name = "sound";
            this.sound.Size = new System.Drawing.Size(329, 24);
            this.sound.TabIndex = 1;
            this.sound.Text = "Play sound when valuable body is detected";
            this.sound.UseVisualStyleBackColor = false;
            this.sound.CheckedChanged += new System.EventHandler(this.saveConfig);
            // 
            // hide
            // 
            this.hide.AutoSize = true;
            this.hide.BackColor = System.Drawing.Color.Transparent;
            this.hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.hide.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.hide.Location = new System.Drawing.Point(12, 138);
            this.hide.Name = "hide";
            this.hide.Size = new System.Drawing.Size(237, 24);
            this.hide.TabIndex = 2;
            this.hide.Text = "Hide overlay after 15 seconds";
            this.hide.UseVisualStyleBackColor = false;
            this.hide.CheckedChanged += new System.EventHandler(this.hide_CheckedChanged);
            // 
            // logo
            // 
            this.logo.AutoSize = true;
            this.logo.BackColor = System.Drawing.Color.Transparent;
            this.logo.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.logo.Location = new System.Drawing.Point(12, 9);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(207, 73);
            this.logo.TabIndex = 8;
            this.logo.Text = "EDEX";
            // 
            // dbb
            // 
            this.dbb.BackColor = System.Drawing.Color.Transparent;
            this.dbb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dbb.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.dbb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.dbb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.dbb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dbb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dbb.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.dbb.Location = new System.Drawing.Point(139, 274);
            this.dbb.Name = "dbb";
            this.dbb.Size = new System.Drawing.Size(189, 33);
            this.dbb.TabIndex = 10;
            this.dbb.Text = "Open Database Builder";
            this.dbb.UseCompatibleTextRendering = true;
            this.dbb.UseVisualStyleBackColor = false;
            this.dbb.Click += new System.EventHandler(this.dbb_Click);
            // 
            // logo2
            // 
            this.logo2.AutoSize = true;
            this.logo2.BackColor = System.Drawing.Color.Transparent;
            this.logo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logo2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.logo2.Location = new System.Drawing.Point(21, 67);
            this.logo2.Name = "logo2";
            this.logo2.Size = new System.Drawing.Size(68, 20);
            this.logo2.TabIndex = 12;
            this.logo2.Text = "Settings";
            // 
            // about
            // 
            this.about.BackColor = System.Drawing.Color.Transparent;
            this.about.Cursor = System.Windows.Forms.Cursors.Hand;
            this.about.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.about.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.about.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.about.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.about.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.about.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.about.Location = new System.Drawing.Point(12, 274);
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(121, 33);
            this.about.TabIndex = 13;
            this.about.Text = "About EDEX";
            this.about.UseCompatibleTextRendering = true;
            this.about.UseVisualStyleBackColor = false;
            this.about.Click += new System.EventHandler(this.about_Click);
            // 
            // donate
            // 
            this.donate.BackColor = System.Drawing.Color.Transparent;
            this.donate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.donate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.donate.FlatAppearance.BorderSize = 0;
            this.donate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.donate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.donate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.donate.Image = ((System.Drawing.Image)(resources.GetObject("donate.Image")));
            this.donate.Location = new System.Drawing.Point(341, 280);
            this.donate.Margin = new System.Windows.Forms.Padding(0);
            this.donate.Name = "donate";
            this.donate.Size = new System.Drawing.Size(76, 23);
            this.donate.TabIndex = 14;
            this.donate.UseVisualStyleBackColor = false;
            this.donate.Click += new System.EventHandler(this.donate_Click);
            // 
            // ver
            // 
            this.ver.AutoSize = true;
            this.ver.BackColor = System.Drawing.Color.Transparent;
            this.ver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ver.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.ver.Location = new System.Drawing.Point(95, 67);
            this.ver.Name = "ver";
            this.ver.Size = new System.Drawing.Size(30, 20);
            this.ver.TabIndex = 15;
            this.ver.Text = "ver";
            // 
            // onlydetect
            // 
            this.onlydetect.AutoSize = true;
            this.onlydetect.BackColor = System.Drawing.Color.Transparent;
            this.onlydetect.Enabled = false;
            this.onlydetect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.onlydetect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.onlydetect.Location = new System.Drawing.Point(12, 168);
            this.onlydetect.Name = "onlydetect";
            this.onlydetect.Size = new System.Drawing.Size(372, 24);
            this.onlydetect.TabIndex = 16;
            this.onlydetect.Text = "Show overlay only if valuable bodies are detected";
            this.onlydetect.UseVisualStyleBackColor = false;
            this.onlydetect.CheckedChanged += new System.EventHandler(this.saveConfig);
            // 
            // transp
            // 
            this.transp.AutoSize = true;
            this.transp.BackColor = System.Drawing.Color.Transparent;
            this.transp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.transp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.transp.Location = new System.Drawing.Point(12, 108);
            this.transp.Name = "transp";
            this.transp.Size = new System.Drawing.Size(202, 24);
            this.transp.TabIndex = 17;
            this.transp.Text = "Make EDEX transparent";
            this.transp.UseVisualStyleBackColor = false;
            this.transp.CheckedChanged += new System.EventHandler(this.saveConfig);
            // 
            // settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(426, 319);
            this.Controls.Add(this.transp);
            this.Controls.Add(this.onlydetect);
            this.Controls.Add(this.ver);
            this.Controls.Add(this.donate);
            this.Controls.Add(this.about);
            this.Controls.Add(this.logo2);
            this.Controls.Add(this.dbb);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.hide);
            this.Controls.Add(this.sound);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EDEX Settings";
            this.Load += new System.EventHandler(this.settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox sound;
        private System.Windows.Forms.CheckBox hide;
        private System.Windows.Forms.Label logo;
        private System.Windows.Forms.Button dbb;
        private System.Windows.Forms.Label logo2;
        private System.Windows.Forms.Button about;
        private System.Windows.Forms.Button donate;
        private System.Windows.Forms.Label ver;
        private System.Windows.Forms.CheckBox onlydetect;
        private System.Windows.Forms.CheckBox transp;
    }
}