﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EDEX
{
    public partial class edexMain : Form
    {

        public static string version = "1.0";




        bool dbScavengerBusy = false;
        string currentSystemName = "";
        string currentLogFile = "";

        string[] settingsFromFile;

        PrivateFontCollection pfc = new PrivateFontCollection();

        FileSystemWatcher watcher, configWatcher;


        Image earthlike = new Bitmap(Properties.Resources.earthlike);
        Image other = new Bitmap(Properties.Resources.other);
        Image ammonia = new Bitmap(Properties.Resources.ammonia);
        Image gas1 = new Bitmap(Properties.Resources.gas1);
        Image highmetal = new Bitmap(Properties.Resources.highmetal);
        Image icy = new Bitmap(Properties.Resources.icy);
        Image metalrich = new Bitmap(Properties.Resources.metalrich);
        Image rocky = new Bitmap(Properties.Resources.rocky);
        Image rockyice = new Bitmap(Properties.Resources.rockyice);
        Image water = new Bitmap(Properties.Resources.water);
        Image more = new Bitmap(Properties.Resources.more);


        //SQLiteConnection db = new SQLiteConnection("Data Source=galaxy.sqlite;Version=3;Journal Mode=Off;Synchronous=Off;Page Size=4096;Cache Size=16777216;");
        SQLiteConnection db = new SQLiteConnection("Data Source=galaxy.sqlite;Version=3;Journal Mode=Off;Synchronous=Off;");


        public void loadFont()
        {
            int fontLength = Properties.Resources.Waukegan_LDO.Length;
            byte[] fontdata = Properties.Resources.Waukegan_LDO;
            System.IntPtr data = Marshal.AllocCoTaskMem(fontLength);
            Marshal.Copy(fontdata, 0, data, fontLength);
            pfc.AddMemoryFont(data, fontLength);

            bodiesValue.Font = new Font(pfc.Families[0], bodiesValue.Font.Size);
            totalv.Font = new Font(pfc.Families[0], totalv.Font.Size);
        }


        public void echo(string tekst)
        {
            console.Invoke((MethodInvoker)delegate
            {
                console.SelectionStart = console.Text.Length;
                console.ScrollToCaret();
                console.SelectedText = tekst + "\n";
                console.SelectionStart = console.Text.Length;
                console.ScrollToCaret();
            });
        }



        public void loadSettings()
        {
            console.Invoke((MethodInvoker)delegate
            {
                StreamReader sr = null;
                try
                {
                    sr = File.OpenText("edex.cfg");
                    sr.ReadLine();
                    settingsFromFile = sr.ReadToEnd().Split(';');

                    if (settingsFromFile[0] == "1")
                        Opacity = .8;
                    else
                        Opacity = 1;

                    if (settingsFromFile[4] == "1")
                    {
                        dbgPanel.Visible = true;
                        Height = 243;
                    }
                    else
                    {
                        dbgPanel.Visible = false;
                        Height = 128;
                    }


                    sr.Close();
                }
                catch (Exception ee)
                {
                    echo(ee + "");
                    try
                    {
                        sr.Close();
                    }
                    catch { }
                }
            });
        }



        public edexMain()
        {
            InitializeComponent();
        }

        private void settings_Click(object sender, EventArgs e)
        {
            settings s = new settings();
            s.Show();
        }

        private void edexMain_Load(object sender, EventArgs e)
        {
            this.Top = 0;
            findCurrentLogFile();
            loadFont();
            loadSettings();

            if(!File.Exists("galaxy.sqlite"))
            {
                MessageBox.Show("Missing database.\nDownload the database or generate one using EDEX Database Builder.\nEDEX will close now.","Missing database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }else if(new System.IO.FileInfo("galaxy.sqlite").Length == 0)
            {
                MessageBox.Show("Database is empty.\nDownload the database or generate one using EDEX Database Builder.\nEDEX will close now.", "Empty database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }



            db.Open();
            CreateFilesystemWatcher();

            try
            {
                WebClient wb = new WebClient();
                string updAval = wb.DownloadString("https://edex.smartmoose.org/system/updates/" + version + ".txt");

                if (updAval == "1")
                {
                    DialogResult dialogResult = MessageBox.Show("Update for EDEX is available. Do you want to get it now?", "Update", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start("https://edex.smartmoose.org/index.php?id=downloads");
                        Application.Exit();
                    }
                }
            }
            catch { }


        }

        private void close_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Exit EDEX?", "Exit", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void checkInput_Click(object sender, EventArgs e)
        {
            currentSystemName = nameInput.Text;

            if (!dbScavengerBusy)
                dbScavenger.RunWorkerAsync();
        }





        public class planet
        {
            public int value;
            public PictureBox picture;
            public Label label;
        };



        private void dbScavenger_DoWork(object sender, DoWorkEventArgs e)
        {
            dbScavengerBusy = true;



            bodiesValue.Invoke((MethodInvoker)delegate
            {
                if (settingsFromFile[2] == "0")
                    Visible = true;

                bodiesContainer.Controls.Clear();
                bodiesValue.Text = "Loading data...";
                totalv.Text = "";
                loading.Visible = true;
            });

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM systems WHERE name='" + currentSystemName.Replace('\'', '-') + "'", db);
            SQLiteDataReader reader = command.ExecuteReader();
            reader.Read();
            Int64 id = 0;

            try
            {
                id = Convert.ToInt64(reader["id"]);
            }
            catch
            {
                bodiesValue.Invoke((MethodInvoker)delegate
                {
                    loading.Visible = false;
                    bodiesValue.Text = "No data on this system.";
                });
                dbScavengerBusy = false;
                return;
            }

            command = new SQLiteCommand("SELECT * FROM bodies WHERE system_id=" + id, db);
            reader = command.ExecuteReader();

            int lastBodyImage = 0;

            int earthlikes = 0;
            int waterworlds = 0;
            int ammoniaworlds = 0;

            int bodiesOverLimit = 0;

            long totalValue = 0;

            List<planet> planets = new List<planet>();

            while (reader.Read())
            {
                planet p = new planet();
                p.picture = new PictureBox()
                {
                    Width = 50,
                    Height = 35
                };



                switch (Convert.ToInt64(reader["type_id"]))
                {
                    case 0:
                        p.picture.Image = other;
                        break;
                    case 20:
                        p.picture.Image = ammonia;
                        ammoniaworlds++;
                        p.value = 320203;
                        break;
                    case 21:
                        p.picture.Image = gas1;
                        p.value = 7013;
                        break;
                    case 22:
                        p.picture.Image = gas1;
                        p.value = 53663;
                        break;
                    case 23:
                        p.picture.Image = gas1;
                        p.value = 2693;
                        break;
                    case 24:
                        p.picture.Image = gas1;
                        p.value = 2799;
                        break;
                    case 25:
                        p.picture.Image = gas1;
                        p.value = 2761;
                        break;
                    case 26:
                        p.picture.Image = earthlike;
                        earthlikes++;
                        p.value = 627885;
                        break;
                    case 27:
                        p.picture.Image = gas1;
                        p.value = 1721;
                        break;
                    case 28:
                        p.picture.Image = gas1;
                        p.value = 2314;
                        break;
                    case 29:
                        p.picture.Image = gas1;
                        p.value = 2095;
                        break;
                    case 30:
                        p.picture.Image = highmetal;
                        p.value = 34310;
                        break;
                    case 31:
                        p.picture.Image = icy;
                        p.value = 1246;
                        break;
                    case 32:
                        p.picture.Image = metalrich;
                        p.value = 65045;
                        break;
                    case 33:
                        p.picture.Image = rocky;
                        p.value = 928;
                        break;
                    case 34:
                        p.picture.Image = rockyice;
                        p.value = 928;
                        break;
                    case 35:
                        p.picture.Image = water;
                        p.value = 1824;
                        break;
                    case 36:
                        p.picture.Image = gas1;
                        p.value = 301410;
                        waterworlds++;
                        break;
                    default:
                        p.picture.Image = other;
                        p.value = 0;
                        break;
                }


                totalValue += p.value;

                p.label = new Label()
                {
                    AutoSize = false,
                    TextAlign = ContentAlignment.TopCenter,
                    Dock = DockStyle.None,

                    Width = 50,
                    Height = 30,
                    ForeColor = Color.FromArgb(233, 129, 40)
                };




                string n = Convert.ToString(reader["name"]).Replace(currentSystemName, string.Empty);


                if (n.Length > 5)
                    p.label.Text = Convert.ToString(n).Substring(0, 4) + "...\r\n" + p.value;
                else
                    p.label.Text = Convert.ToString(n) + "\r\n" + p.value;


                if (Convert.ToInt64(reader["type_id"]) != 0)
                {
                    //add to list
                    planets.Add(p);
                }

            }



            //sort the list
            planets.Sort(delegate (planet p1, planet p2) { return p2.value.CompareTo(p1.value); });

            bodiesValue.Invoke((MethodInvoker)delegate
            {


                foreach (planet b in planets)
                {
                    //skip starts, belts etc
                    if (b.value == 0)
                        continue;

                    if (lastBodyImage < 20)
                    {
                        b.picture.Location = new Point((lastBodyImage * 50) + 7, 0);
                        b.label.Location = new Point((lastBodyImage * 50), 40);

                        bodiesContainer.Controls.Add(b.picture);
                        bodiesContainer.Controls.Add(b.label);
                    }
                    else
                    {
                        bodiesOverLimit++;
                    }


                    lastBodyImage++;

                }



                if (bodiesOverLimit != 0)
                {
                    Label t = new Label()
                    {
                        AutoSize = false,
                        TextAlign = ContentAlignment.TopCenter,
                        Dock = DockStyle.None,

                        Text = bodiesOverLimit + "\nmore",
                        Location = new Point(1000, 40),
                        Width = 50,
                        Height = 30,
                        ForeColor = Color.FromArgb(233, 129, 40)
                    };

                    PictureBox p = new PictureBox()
                    {
                        Width = 50,
                        Height = 35,
                        Location = new Point(1007, 0),
                        Image = more
                    };

                    bodiesContainer.Controls.Add(t);
                    bodiesContainer.Controls.Add(p);
                }



















                if (earthlikes != 0 || waterworlds != 0 || ammoniaworlds != 0)
                {
                    bodiesValue.Text = "";
                    if (earthlikes == 1)
                        bodiesValue.Text += "Found 1 Earth-like planet! ";
                    else if (earthlikes > 1)
                        bodiesValue.Text += "Found " + earthlikes + " Earth-like planets! ";

                    if (waterworlds == 1)
                        bodiesValue.Text += "Found 1 Water World! ";
                    else if (waterworlds > 1)
                        bodiesValue.Text += "Found " + waterworlds + " Water Worlds! ";

                    if (ammoniaworlds == 1)
                        bodiesValue.Text += "Found 1 Ammonia World! ";
                    else if (ammoniaworlds > 1)
                        bodiesValue.Text += "Found " + ammoniaworlds + " Ammonia Worlds! ";


                    Visible = true;

                    if (settingsFromFile[3] == "1")
                    {
                        System.IO.Stream str = Properties.Resources.beep;
                        System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
                        snd.Play();
                    }

                }
                else
                {
                    bodiesValue.Text = "No high value bodies detected";
                }

                totalv.Text = "Total value of bodies in this system: " + totalValue.ToString("#,##0") + " credits.";

                loading.Visible = false;

                if (settingsFromFile[1] == "1")
                    hidder.Start();
            });


            dbScavengerBusy = false;
        }




        private void hidder_Tick(object sender, EventArgs e)
        {
            if (settingsFromFile[1] == "1")
                Visible = false;

            hidder.Stop();
        }




        public void findCurrentLogFile()
        {
            string dirPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Saved Games\Frontier Developments\Elite Dangerous";
            var directory = new DirectoryInfo(dirPath);
            currentLogFile = dirPath + "\\" + Convert.ToString(directory.GetFiles().OrderByDescending(f => f.LastWriteTime).First());
        }


        public void findCurrentSystem()
        {
            bool success = false;
            while (!success)
            {

                echo("Finding system from file: "+ currentLogFile);

                FileStream stream=null;
                try
                {
                    System.Threading.Thread.Sleep(500);

                    //WebClient client = new WebClient();
                    //Stream stream = client.OpenRead(currentLogFile);
                    stream = new FileStream(currentLogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    StreamReader streamReader = new StreamReader(stream);
                    JsonTextReader reader = new JsonTextReader(streamReader);

                    reader.SupportMultipleContent = true;

                    var serializer = new JsonSerializer();
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader.TokenType == JsonToken.StartObject)
                            {
                                GameEvent c = serializer.Deserialize<GameEvent>(reader);


                                if (c.JumpType == "Hyperspace")
                                {
                                    echo(c.StarSystem);
                                    currentSystemName = c.StarSystem;
                                }

                            }
                        }

                        success = true;
                    }
                    catch { }

                }
                catch (Exception ee)
                {
                    try
                    {
                        stream.Close(); //if it died, let's close it to be sure
                    }
                    catch { }
                    echo("FS Exception: " + ee);
                }
            }
        }







        public void CreateFilesystemWatcher()
        {
            watcher = new FileSystemWatcher();
            watcher.Path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Saved Games\Frontier Developments\Elite Dangerous";
            watcher.NotifyFilter = NotifyFilters.Attributes |
                NotifyFilters.CreationTime |
                NotifyFilters.FileName |
                NotifyFilters.LastAccess |
                NotifyFilters.LastWrite |
                NotifyFilters.Size |
                NotifyFilters.Security;

            watcher.Filter = "*.log";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnCreated);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnChanged);

            // Begin watching.
            watcher.EnableRaisingEvents = true;











            configWatcher = new FileSystemWatcher();

            configWatcher.Path = AppDomain.CurrentDomain.BaseDirectory;
            configWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            configWatcher.Filter = "edex.cfg";

            configWatcher.Changed += new FileSystemEventHandler(configOnChanged);
            configWatcher.Created += new FileSystemEventHandler(configOnChanged);
            configWatcher.Renamed += new RenamedEventHandler(configOnChanged);

            configWatcher.EnableRaisingEvents = true;
        }

        private void configOnChanged(object source, FileSystemEventArgs e)
        {
            loadSettings();
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            string prevSystem = currentSystemName;
            echo("OnChanged()\nSystem before: " + currentSystemName);
            findCurrentSystem();
            echo("System after: " + currentSystemName);

            if (!dbScavengerBusy && prevSystem != currentSystemName)
                dbScavenger.RunWorkerAsync();
        }

        private void OnCreated(object source, FileSystemEventArgs e)
        {
            echo("OnCreated()\nFile before: " + currentLogFile);
            findCurrentLogFile();
            echo("File before: " + currentLogFile);
        }

        private void simjump_Click(object sender, EventArgs e)
        {
            OnChanged(null, null);
        }

        private void fsRefresh_Tick(object sender, EventArgs e)
        {
            //we need that to fire once in a while
            //cause windows is caching ED log file before writting it to HDD
            //and we can't detect changes in it with FileSystemWatcher (congrats MS...).
            //refreshing FileInfo flushes windows' cache
            //making the changes detectable

            FileInfo f = new FileInfo(currentLogFile);
            f.Refresh();
        }

        private void simrun_Click(object sender, EventArgs e)
        {
            OnCreated(null, null);

        }
    }
}





class GameEvent
{
    public string timestamp { get; set; }
    public string JumpType { get; set; }
    public string StarSystem { get; set; }
}
