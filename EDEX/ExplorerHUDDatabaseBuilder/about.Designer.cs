﻿namespace EDEX
{
    partial class about
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(about));
            this.thanks = new System.Windows.Forms.RichTextBox();
            this.logo2 = new System.Windows.Forms.Label();
            this.logo = new System.Windows.Forms.Label();
            this.www = new System.Windows.Forms.Button();
            this.mail = new System.Windows.Forms.Button();
            this.www2 = new System.Windows.Forms.Button();
            this.ver = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // thanks
            // 
            this.thanks.BackColor = System.Drawing.Color.Black;
            this.thanks.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.thanks.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.thanks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.thanks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.thanks.Location = new System.Drawing.Point(12, 344);
            this.thanks.Name = "thanks";
            this.thanks.ReadOnly = true;
            this.thanks.Size = new System.Drawing.Size(1082, 168);
            this.thanks.TabIndex = 0;
            this.thanks.Text = resources.GetString("thanks.Text");
            this.thanks.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.thanks_LinkClicked);
            // 
            // logo2
            // 
            this.logo2.AutoSize = true;
            this.logo2.BackColor = System.Drawing.Color.Transparent;
            this.logo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logo2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.logo2.Location = new System.Drawing.Point(21, 67);
            this.logo2.Name = "logo2";
            this.logo2.Size = new System.Drawing.Size(169, 40);
            this.logo2.TabIndex = 10;
            this.logo2.Text = "Explorer\'s Companion\r\nby SmartMoose Group";
            // 
            // logo
            // 
            this.logo.AutoSize = true;
            this.logo.BackColor = System.Drawing.Color.Transparent;
            this.logo.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.logo.Location = new System.Drawing.Point(12, 9);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(207, 73);
            this.logo.TabIndex = 9;
            this.logo.Text = "EDEX";
            // 
            // www
            // 
            this.www.BackColor = System.Drawing.Color.Transparent;
            this.www.Cursor = System.Windows.Forms.Cursors.Hand;
            this.www.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.www.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.www.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.www.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.www.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.www.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.www.Location = new System.Drawing.Point(924, 12);
            this.www.Name = "www";
            this.www.Size = new System.Drawing.Size(170, 33);
            this.www.TabIndex = 14;
            this.www.Text = "Visit EDEX Website";
            this.www.UseCompatibleTextRendering = true;
            this.www.UseVisualStyleBackColor = false;
            this.www.Click += new System.EventHandler(this.www_Click);
            // 
            // mail
            // 
            this.mail.BackColor = System.Drawing.Color.Transparent;
            this.mail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mail.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.mail.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.mail.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.mail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.mail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.mail.Location = new System.Drawing.Point(924, 90);
            this.mail.Name = "mail";
            this.mail.Size = new System.Drawing.Size(170, 33);
            this.mail.TabIndex = 15;
            this.mail.Text = "Send us Email";
            this.mail.UseCompatibleTextRendering = true;
            this.mail.UseVisualStyleBackColor = false;
            this.mail.Click += new System.EventHandler(this.mail_Click);
            // 
            // www2
            // 
            this.www2.BackColor = System.Drawing.Color.Transparent;
            this.www2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.www2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.www2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.www2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.www2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.www2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.www2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.www2.Location = new System.Drawing.Point(924, 51);
            this.www2.Name = "www2";
            this.www2.Size = new System.Drawing.Size(170, 33);
            this.www2.TabIndex = 16;
            this.www2.Text = "Visit SM Website";
            this.www2.UseCompatibleTextRendering = true;
            this.www2.UseVisualStyleBackColor = false;
            this.www2.Click += new System.EventHandler(this.www2_Click);
            // 
            // ver
            // 
            this.ver.AutoSize = true;
            this.ver.BackColor = System.Drawing.Color.Transparent;
            this.ver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ver.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(129)))), ((int)(((byte)(40)))));
            this.ver.Location = new System.Drawing.Point(21, 107);
            this.ver.Name = "ver";
            this.ver.Size = new System.Drawing.Size(30, 20);
            this.ver.TabIndex = 17;
            this.ver.Text = "ver";
            // 
            // about
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1106, 524);
            this.Controls.Add(this.ver);
            this.Controls.Add(this.www2);
            this.Controls.Add(this.mail);
            this.Controls.Add(this.www);
            this.Controls.Add(this.logo2);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.thanks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "about";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About EDEX";
            this.Load += new System.EventHandler(this.about_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox thanks;
        private System.Windows.Forms.Label logo2;
        private System.Windows.Forms.Label logo;
        private System.Windows.Forms.Button www;
        private System.Windows.Forms.Button mail;
        private System.Windows.Forms.Button www2;
        private System.Windows.Forms.Label ver;
    }
}