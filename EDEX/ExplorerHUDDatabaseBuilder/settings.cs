﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EDEX
{
    public partial class settings : Form
    {

        PrivateFontCollection pfc = new PrivateFontCollection();

        bool stillLoading = true;
        public void loadFont()
        {
            int fontLength = Properties.Resources.Waukegan_LDO.Length;
            byte[] fontdata = Properties.Resources.Waukegan_LDO;
            System.IntPtr data = Marshal.AllocCoTaskMem(fontLength);
            Marshal.Copy(fontdata, 0, data, fontLength);
            pfc.AddMemoryFont(data, fontLength);

            logo.Font = new Font(pfc.Families[0], logo.Font.Size);
            logo2.Font = new Font(pfc.Families[0], logo2.Font.Size);
            sound.Font = new Font(pfc.Families[0], sound.Font.Size);
            hide.Font = new Font(pfc.Families[0], hide.Font.Size);
            dbb.Font = new Font(pfc.Families[0], dbb.Font.Size);
            ver.Font = new Font(pfc.Families[0], ver.Font.Size);
            onlydetect.Font = new Font(pfc.Families[0], onlydetect.Font.Size);
            about.Font = new Font(pfc.Families[0], about.Font.Size);
            transp.Font = new Font(pfc.Families[0], about.Font.Size);
        }


        public settings()
        {
            InitializeComponent();
        }

        private void dbb_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"EDEX Database Builder.exe");

        }

        private void settings_Load(object sender, EventArgs e)
        {
            loadFont();
            loadSettings();

            ver.Text = "Version "+edexMain.version;
        }


        public void loadSettings()
        {
            StreamReader sr=null;
            try
            {
                sr = File.OpenText("edex.cfg");
                sr.ReadLine();
                string[] s = sr.ReadToEnd().Split(';');

                if (s[0] == "1")
                    transp.Checked = true;

                if (s[1] == "1")
                    hide.Checked = true;

                if (s[2] == "1")
                    onlydetect.Checked = true;

                if (s[3] == "1")
                    sound.Checked = true;

                sr.Close();
            }
            catch {
                try
                {
                    sr.Close();
                }
                catch { }
            }

            stillLoading = false;
        }

        private void about_Click(object sender, EventArgs e)
        {
            about a = new about();
            a.Show();
        }

        private void donate_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2JRFEC9R58NTW");
        }

        private void hide_CheckedChanged(object sender, EventArgs e)
        {
            onlydetect.Enabled = hide.Checked;

            if (!onlydetect.Enabled)
                onlydetect.Checked = false;

            saveConfig(sender, e);
        }



        private void saveConfig(object sender, EventArgs e)
        {
            if (stillLoading)
                return;

            string set = "[0=off, 1=on] UI transparency;Hide after 15s;Show only on valuable detection;Enable sound;Show debug panel\n";

            if (transp.Checked)
                set += "1;";
            else
                set += "0;";

            if (hide.Checked)
                set += "1;";
            else
                set += "0;";

            if (onlydetect.Checked)
                set += "1;";
            else
                set += "0;";

            if (sound.Checked)
                set += "1;";
            else
                set += "0;";

            set += "0";

            try
            {
                /*if (File.Exists("edex.cfg"))
                    File.Delete("edex.cfg");*/
                StreamWriter sw = File.CreateText("edex.cfg");
                sw.Write(set);
                sw.Close();
            }
            catch
            {
                MessageBox.Show("Couldn't save settings.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
