﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EDEX
{
    public partial class about : Form
    {


        PrivateFontCollection pfc = new PrivateFontCollection();

        public void loadFont()
        {
            int fontLength = Properties.Resources.Waukegan_LDO.Length;
            byte[] fontdata = Properties.Resources.Waukegan_LDO;
            System.IntPtr data = Marshal.AllocCoTaskMem(fontLength);
            Marshal.Copy(fontdata, 0, data, fontLength);
            pfc.AddMemoryFont(data, fontLength);

            logo.Font = new Font(pfc.Families[0], logo.Font.Size);
            logo2.Font = new Font(pfc.Families[0], logo2.Font.Size);
            mail.Font = new Font(pfc.Families[0], mail.Font.Size);
            www.Font = new Font(pfc.Families[0], www.Font.Size);
            www2.Font = new Font(pfc.Families[0], www2.Font.Size);
            ver.Font = new Font(pfc.Families[0], ver.Font.Size);

        }

        public about()
        {
            InitializeComponent();
        }

        private void thanks_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }

        private void about_Load(object sender, EventArgs e)
        {
            loadFont();
            ver.Text = "Version "+edexMain.version;
        }

        private void www_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://edex.smartmoose.org");
        }

        private void mail_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:contact@smartmoose.org");
        }

        private void www2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://smartmoose.org");
        }
    }
}
