; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "EDEX"
#define MyAppVersion "1.0"
#define MyAppPublisher "SmartMoose"
#define MyAppURL "http://smartmoose.org"
#define MyAppExeName "EDEX.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{6E6E427E-BE5B-405A-B0F0-93A9FFCCF71E}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
OutputDir=C:\Users\pionner\Desktop\edexinstall
OutputBaseFilename=EDEX_Setup
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "D:\GIT\EDEX\EDEX\full\EDEX.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\GIT\EDEX\EDEX\full\EDEX Database Builder.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\GIT\EDEX\EDEX\full\edex.cfg"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\GIT\EDEX\EDEX\full\galaxy.sqlite"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\GIT\EDEX\EDEX\full\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\GIT\EDEX\EDEX\full\System.Data.SQLite.dll"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\EDEX Database Builder"; Filename: "{app}\EDEX Database Builder.exe"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

